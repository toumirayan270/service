import React from "react";
import {BrowserRouter as Router,Link,Route,Routes} from 'react-router-dom';
import Services from './Services';
import Employees from './Employees';
import AddService from './AddService';
import AddEmployee from './AddEmployee';
import ServiceDetails from './ServiceDetails';
import { useState,useEffect } from "react";
import axios from "axios";

function SideBare(){
    let [services,setServices] = useState([]);
    useEffect(()=>{
     axios.get("http://localhost:3006/services")
    .then(json=>{
      setServices(json.data);
    });
  },[]);
    return(
    <Router>
    <div >


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">

  
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      
      <li class="nav-item">
      <Link className="nav-link" aria-current="page" to="/">Services</Link>
      </li>
      <li>
        <Link className="nav-link" to="/employees">Employees</Link>
      </li>
      
    </ul>
  </div>
</nav>

<div id="page-content-wrapper" className="bg ">
    <div className="container-fluid ">
    <Routes>
            <Route path="/" element={<Services services={services} />}/>
            <Route path="/employees" element={<Employees services={services}/>}/>
            <Route path="/service/:id" element={<ServiceDetails services={services}/>}/>
            <Route path="/add_service" element={<AddService  setServices={setServices}/>}/>
            <Route path="/add_employee/:code" element={<AddEmployee services={services}/>}/>
        </Routes>
    </div>
</div>

</div>
</Router>
    )

}




export default SideBare;